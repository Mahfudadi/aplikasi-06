package saputro.mahfud.appx6

import android.app.AlertDialog
import android.content.DialogInterface
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.CursorAdapter
import android.widget.ListAdapter
import android.widget.SimpleCursorAdapter
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.frag_data_nilai.*
import kotlinx.android.synthetic.main.frag_data_nilai.view.*

class FragmentNilai : Fragment(), View.OnClickListener{

    val MhsSelect : AdapterView.OnItemSelectedListener = object : AdapterView.OnItemSelectedListener{
        override fun onNothingSelected(parent: AdapterView<*>?) {
            spinMhs.setSelection(0,true)
        }

        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            val c : Cursor = spAdapter.getItem(position) as Cursor
            namaMhs = c.getString(c.getColumnIndex("_id"))
        }
    }

    val MatkulSelect : AdapterView.OnItemSelectedListener = object : AdapterView.OnItemSelectedListener{
        override fun onNothingSelected(parent: AdapterView<*>?) {
            spinMatkul.setSelection(0,true)
        }

        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            val c : Cursor = spAdapter2.getItem(position) as Cursor
            namaMatkul = c.getString(c.getColumnIndex("_id"))
        }
    }

    lateinit var thisParent : MainActivity
    lateinit var lsAdapter : ListAdapter
    lateinit var spAdapter : SimpleCursorAdapter
    lateinit var spAdapter2 : SimpleCursorAdapter
    lateinit var dialog: AlertDialog.Builder
    lateinit var v : View
    var namaMhs : String = ""
    var namaMatkul : String = ""
    lateinit var db : SQLiteDatabase

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        thisParent = activity as MainActivity
        v = inflater.inflate(R.layout.frag_data_nilai,container,false)
        db = thisParent.getDbObject()
        dialog = AlertDialog.Builder(thisParent)
        v.btnDeleteNilai.setOnClickListener(this)
        v.btnInsertNilai.setOnClickListener(this)
        v.btnUpdateNilai.setOnClickListener(this)
        v.spinMhs.onItemSelectedListener = MhsSelect
        v.spinMatkul.onItemSelectedListener = MatkulSelect
        return v
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnDeleteNilai->{

            }
            R.id.btnUpdateNilai->{

            }
            R.id.btnInsertNilai->{
                dialog.setTitle("Konfirmasi").setIcon(android.R.drawable.ic_dialog_info)
                    .setMessage("Apakah data yang akan dimasukkan sudah benar?")
                    .setPositiveButton("Ya",btnInsertDialog)
                    .setNegativeButton("Tidak", null)
                dialog.show()
            }
        }
    }

    override fun onStart() {
        super.onStart()
        showDataNilai()
        showDataMhs()
        showDataMatkul()
    }

    fun showDataNilai(){
        var sql="select n.id_nilai as _id, m.nama, mk.matkul, n.nilai from tnilai n, mhs m, tmatkul mk " +
                " where n.nim = m.nim and n.id_matkul=mk.id_matkul"
        var c : Cursor = db.rawQuery(sql,null)
        lsAdapter = SimpleCursorAdapter(thisParent,R.layout.item_data_nilai,c,
            arrayOf("_id","nama","matkul","nilai"), intArrayOf(R.id.txIdNilai, R.id.txNmMhs, R.id.txNamaMatkul, R.id.txNilai),
            CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        v.lsNilai.adapter = lsAdapter

    }

    fun showDataMhs(){
        val c : Cursor = db.rawQuery("select nama as _id from mhs order by nama asc",null)
        spAdapter = SimpleCursorAdapter(thisParent, android.R.layout.simple_spinner_item,c,
            arrayOf("_id"), intArrayOf(android.R.id.text1),CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        spAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        v.spinMhs.adapter = spAdapter
        v.spinMhs.setSelection(0)
    }

    fun showDataMatkul(){
        val c : Cursor = db.rawQuery("select matkul as _id from tmatkul order by matkul asc",null)
        spAdapter2 = SimpleCursorAdapter(thisParent, android.R.layout.simple_spinner_item,c,
            arrayOf("_id"), intArrayOf(android.R.id.text1),CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        spAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        v.spinMatkul.adapter = spAdapter2
        v.spinMatkul.setSelection(0)
    }

    fun insertDataNilai(nim : String, id_matkul : String, nilai: String){
        var sql = "insert into tnilai(nim, id_matkul, nilai) values (?,?,?)"
        db.execSQL(sql, arrayOf(nim,id_matkul,nilai))
        showDataNilai()
    }

    val btnInsertDialog = DialogInterface.OnClickListener { dialog, which ->
        var sql = "select nim from mhs where nama='$namaMhs'"
        var sql2 = "select id_matkul from tmatkul where matkul='$namaMatkul'"
        val c : Cursor = db.rawQuery(sql,null)
        val c2 : Cursor = db.rawQuery(sql2,null)
        if(c.count>0 && c2.count>0){
            c.moveToFirst()
            c2.moveToFirst()
            insertDataNilai(
                c.getString(c.getColumnIndex("nim")),
                c2.getString(c2.getColumnIndex("id_matkul")),
                v.edNilaiMhs.text.toString()
            )
            v.spinMhs.setSelection(0)
            v.spinMatkul.setSelection(0)
            v.edNilaiMhs.setText("")
        }
    }

}