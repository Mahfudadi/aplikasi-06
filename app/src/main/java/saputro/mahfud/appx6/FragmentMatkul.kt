package saputro.mahfud.appx6

import android.app.AlertDialog
import android.content.DialogInterface
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ListAdapter
import android.widget.SimpleCursorAdapter
import androidx.cursoradapter.widget.CursorAdapter
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.frag_data_matkul.*
import kotlinx.android.synthetic.main.frag_data_matkul.view.*
import kotlinx.android.synthetic.main.frag_data_matkul.view.edNamaMatkul
import kotlinx.android.synthetic.main.frag_data_matkul.view.spMatkul
import kotlinx.android.synthetic.main.item_data_matkul.view.*

class FragmentMatkul : Fragment(), View.OnClickListener, AdapterView.OnItemSelectedListener {

    override fun onNothingSelected(parent: AdapterView<*>?) {
        spMatkul.setSelection(0,true)
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        val c: Cursor = spAdapter.getItem(position) as Cursor
        namaProdi = c.getString(c.getColumnIndex("_id"))
    }

    lateinit var thisParent: MainActivity
    lateinit var lsAdapter: ListAdapter
    lateinit var spAdapter: SimpleCursorAdapter
    lateinit var dialog: AlertDialog.Builder
    lateinit var v: View
    var namaProdi: String = ""
    lateinit var db: SQLiteDatabase

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        thisParent = activity as MainActivity
        v = inflater.inflate(R.layout.frag_data_matkul, container, false)
        db = thisParent.getDbObject()
        dialog = AlertDialog.Builder(thisParent)
        v.btnDeleteMatkul.setOnClickListener(this)
        v.btnInsertMatkul.setOnClickListener(this)
        v.btnUpdateMatkul.setOnClickListener(this)
        v.spMatkul.onItemSelectedListener = this
        return v
    }

    override fun onStart() {
        super.onStart()
        showDataMatkul()
        showDataProdi()
    }

    fun showDataMatkul() {
        var sql =  "select tm.id_matkul as _id, tm.matkul, p.nama_prodi from tmatkul tm, prodi p " +
                    "where tm.id_prodi = p.id_prodi order by _id asc"
        val c: Cursor = db.rawQuery(sql, null)
        lsAdapter = SimpleCursorAdapter(thisParent, R.layout.item_data_matkul, c,
            arrayOf("_id", "matkul", "nama_prodi"), intArrayOf(R.id.edIdMatkul, R.id.txmtkl, R.id.txNamaPr),
            CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER
        )
        v.lsMatkul.adapter = lsAdapter
    }

    fun showDataProdi() {
        val c: Cursor = db.rawQuery("select nama_prodi _id from prodi order by nama_prodi asc", null)
        spAdapter = SimpleCursorAdapter(thisParent, android.R.layout.simple_spinner_item, c,
            arrayOf("_id"), intArrayOf(android.R.id.text1), CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        spAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        v.spMatkul.adapter = spAdapter
        v.spMatkul.setSelection(0)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btnDeleteMatkul -> {
//                dialog.setTitle("Konfirmasi").setIcon(android.R.drawable.ic_dialog_info)
//                    .setMessage("Apakah data yang akan dimasukkan sudah benar?")
//                    .setPositiveButton("Ya",btnDeleteDialog)
//                    .setNegativeButton("Tidak",null)
//                dialog.show()
            }
            R.id.btnUpdateMatkul -> {
//                dialog.setTitle("Konfirmasi").setIcon(android.R.drawable.ic_dialog_info)
//                    .setMessage("Apakah data yang akan dimasukkan sudah benar?")
//                    .setPositiveButton("Ya",btnUpdateDialog)
//                    .setNegativeButton("Tidak",null)
//                dialog.show()
            }
            R.id.btnInsertMatkul -> {
                dialog.setTitle("Konfirmasi").setIcon(android.R.drawable.ic_dialog_info)
                    .setMessage("Apakah data yang akan dimasukkan sudah benar?")
                    .setPositiveButton("Ya", btnInsertDialog)
                    .setNegativeButton("Tidak", null)
                dialog.show()
            }
        }
    }

    fun insertDataMatkul(id_matkul: String, Matkul: String, id_prodi: Int) {
        var sql = "insert into tmatkul(id_matkul, matkul, id_prodi) values (?,?,?)"
        db.execSQL(sql, arrayOf(id_matkul, Matkul, id_prodi))
        showDataMatkul()
    }

    val btnInsertDialog = DialogInterface.OnClickListener { dialog, which ->
        var sql = "select id_prodi from prodi where nama_prodi='$namaProdi'"
        val c: Cursor = db.rawQuery(sql, null)
        if (c.count > 0) {
            c.moveToFirst()
            insertDataMatkul(
                v.edMatkul.text.toString(),
                v.edNamaMatkul.text.toString(),
                c.getInt(c.getColumnIndex("id_prodi"))
            )
            v.edMatkul.setText("")
            v.edNamaMatkul.setText("")
        }

    }
}