package saputro.mahfud.appx6

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.frag_perkalian.*
import kotlinx.android.synthetic.main.frag_perkalian.view.*
import java.text.DecimalFormat

class FragmentPerkalian : Fragment(), View.OnClickListener {

    var x : Double = 0.0
    var y : Double = 0.0
    var hasil : Double = 0.0
    lateinit var v : View

    lateinit var thisParent : MainActivity
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        thisParent = activity as MainActivity
        v = inflater.inflate(R.layout.frag_perkalian,container,false)
        v.btnBagi.setOnClickListener(this)
        return v
    }

    override fun onClick(v: View?) {
        x = edX.text.toString().toDouble()
        y = edY.text.toString().toDouble()
        hasil = x*y
        txHasilBagi.text = DecimalFormat("#.##").format(hasil)
    }

}